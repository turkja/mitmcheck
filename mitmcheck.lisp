

;;;; mitmcheck server POC

(ql:quickload 'sb-aclrepl)
(ql:quickload 'usocket)
(ql:quickload 'cl+ssl)
(ql:quickload 'ironclad)
(ql:quickload 'drakma)
(ql:quickload 'hunchentoot)
(ql:quickload 'split-sequence)
(ql:quickload 'swank :silent t)
(ql:quickload 'binascii)
(ql:quickload 'local-time)


;;; cl+ssl extensions

(cffi:defcfun ("i2d_X509" i2d-x509)
    :int
  (x :pointer)
  (*out :pointer))

(cffi:defcfun ("CRYPTO_free" crypto-free)
    :void
  (addr :pointer))



;;; Default configuration

(defvar *port* 24420)
(defvar *address* "127.0.0.1")
(defvar *key* nil)
(defvar *cert* nil)
(defvar *access-log* nil)
(defvar *message-log* nil)
(defvar *upstream-proxy* nil)
(defvar *upstream-server* nil)
(defvar *upstream-cert* nil)
(defvar *upstream-cache-ttl* (* 24 60 60)) ; default TTL = 24h
(defvar *direct-cache-ttl* 300)  ; default TTL for direct queries = 5m
(defvar *verdict-callback* nil)
(defvar *proxy-callback* nil)
(defvar *digest-callback* nil)
(defvar *cipher-list* cl+ssl:*default-cipher-list*)

;;; In-memory cert store: mapping between sha256 digest and DER
;;; Note: EQUALP hash table since the keys are uint8 vectors
(defparameter *cert-hash-table* (make-hash-table :test 'equalp))

;;; Server start timestamp
(defvar *server-start-timestamp* 0)

;;; Upstream cache
(defparameter *upstream-cache-table* (make-hash-table :test 'equal))


;;; Verdict is basically very simple: if there are 2 (or more)
;;; digests, they all need to be identical for Trusted verdict.

(defun get-verdict (hashes)
  "Get verdict of the hash list."
  (let ((h (split-sequence:split-sequence #\; hashes)))
    (if (or (< (length h) 2) (string= "" (first h)))
	nil
	(every #'equal h (rest h)))))

(defun convert-certificate (cert)
  "Concert certificate from internal OpenSSL format to DER."
  (let* ((buf (cffi:foreign-alloc :pointer))
	 (len 0))
    (setf (cffi:mem-ref buf :pointer) (cffi:null-pointer))
    ;; NOTE: buf needs to be freed with crypto-free by caller
    ;; (unless error is signaled)
    (setf len (i2d-x509 cert buf))
    (if (< len 0)
	(progn
	  (cffi:foreign-free buf)
	  (error "Cannot convert X509->DER")))
    (values len buf)))


(defun get-digest (ssl-stream)
  "Get digest from SSL stream."
  (let* ((ssl (cl+ssl::ssl-stream-handle ssl-stream))
	 (cert (cl+ssl::ssl-get-peer-certificate ssl))
	 (sha (ironclad:make-digest :sha256))
	 (digest nil))
    (unless (cffi:null-pointer-p cert)
      (multiple-value-bind (len buf)
	  (convert-certificate cert)

	(let ((der (make-array len :element-type '(unsigned-byte 8) :initial-element 0)))
	  ;; Copy data from C
	  (dotimes (i len)
	    (setf (aref der i)
		  (cffi:mem-aref (cffi:mem-ref buf :pointer) :uint8 i)))

	  ;; Calculate SHA256
	  (setf digest (ironclad:digest-sequence sha der))
	  ;; Store to in-memory hash table
	  (setf (gethash digest *cert-hash-table*) der))
	  	
	;; Free OpenSSL resources
	(crypto-free (cffi:mem-ref buf :pointer))
	(cffi:foreign-free buf))
      (cl+ssl::x509-free cert))

    ;; Return a hex-presentation of the cert:
    (string-upcase (binascii:encode-hex digest))))


;;; Connect to server, return SHA256 of the cerficate in DER format

(defun get-cert-digest (&key
			  (host nil)
			  (port 443)
			  (proxy-host nil)
			  (proxy-port 8118))
  ;; Set the real host and port
  (let ((rhost host)(rport port))
    (when proxy-host
      (setf rhost proxy-host)
      (setf rport proxy-port))
    ;; Connect
    (usocket:with-client-socket (socket socket-stream rhost rport)
      ;; If proxy, send CONNECT to the socket
      (when proxy-host
	(format socket-stream
		"CONNECT https://~a:~a HTTP/1.1~%Host: ~a:~a~%~%"
		host port host port)
	(force-output socket-stream)
	(read-line socket-stream))

      ;; Now the channel shoud be ready to wrap in SSL
      (let ((https
	     (progn
	       (cl+ssl:make-ssl-client-stream
		socket-stream
		:cipher-list *cipher-list*
		:method 'ssl-TLSv1-2-method
		:verify nil
		:hostname host
		:close-callback (lambda () (usocket:socket-close socket))
		:unwrap-stream-p t
		:external-format '(:iso-8859-1 :eol-style :lf))))
	    (digest nil))

	;; Get hash of the certificate and just drop the connection
	(setf digest (get-digest https))
	(close https)
	digest))))

(defun get-digest-direct (url)
  "Get certificate digest with direct connection."
  (let* ((pobj (puri:parse-uri url))
	 (host (puri:uri-host pobj))
	 (port (puri:uri-port pobj))
	 (proxy (when *proxy-callback*
		  (funcall *proxy-callback* url)))
	 (pobj2 (puri:parse-uri proxy))
	 (proxy-host (puri:uri-host pobj2))
	 (proxy-port (puri:uri-port pobj2)))

    (if (null port)
	(setf port 443))
    (if (null proxy-port)
	(setf proxy-port 8080))

    ;; Try to play with SSL cipher set
    ;; XXX: it makes no difference!
    (cl+ssl:with-global-context ((cl+ssl:make-context
				  :verify-mode cl+ssl:+SSL-VERIFY-NONE+)
				 :auto-free-p t)
      (handler-case
	  (get-cert-digest :host host
			   :port port
			   :proxy-host proxy-host
			   :proxy-port proxy-port)
	(error (ex) (progn
		      (format t "Error: ~a" ex)
		      nil))))))


;;; Upstream result caching

(defun show-upstream-cache (&optional (url nil))
  (let ((now (local-time:timestamp-to-unix (local-time:now))))
    (loop for key being the hash-keys of *upstream-cache-table*
       do
	 (if (or (null url) (and url (search url key)))
	     (let* ((entry (gethash key *upstream-cache-table*))
		    (then (first entry))
		    (cache (first (rest entry)))
		    (ttl (- *upstream-cache-ttl* (- now then))))
	       (if (> ttl 0)
		   (format t "\"~a\"~%  - ~a - ttl:~d~%"
		       key (drakma:header-value :HASH-CHAIN (third cache)) ttl)))))))

(defun clear-cache ()
  (clrhash *upstream-cache-table*))

(defun remove-from-cache (url)
  (if (gethash url *upstream-cache-table*)
      (remhash url *upstream-cache-table*))
  nil)

(defun add-to-cache (url data)
  (let ((now (local-time:timestamp-to-unix (local-time:now))))
    (unless (gethash url *upstream-cache-table*)
      (setf (gethash url *upstream-cache-table*) (list now data)))))

(defun check-cache (url)
  (let ((now (local-time:timestamp-to-unix (local-time:now)))
	(cache-entry (gethash url *upstream-cache-table*)))
    (when cache-entry
      ;; cache hit
      (if (< (- now (first cache-entry))
	     *upstream-cache-ttl*)
	  (second cache-entry)
	  (remove-from-cache url)))))


;;; Query upstream, or return from local cache

(defun query-upstream (url)
  "Get certificate digest from upstream server."
  (let ((ce (check-cache url))
	(re nil))
    (if ce
	(values-list (rest ce))
	(handler-case
	    (progn
	      (setf re (multiple-value-list
			(drakma:http-request
			 (concatenate 'string *upstream-server*
				      "/mitmcheck?url=" url)
			 :ca-file *upstream-cert*
			 :proxy *upstream-proxy*)))	      
	      (add-to-cache url re)
	      (values-list (rest re)))
	  (error (ex) (progn
			(format t "Error: ~a~%" ex)
			nil))))))

(defun normalize-url (url)
  "Some sanity checking for the url."
  (if (or (null url) (string= "" url))
      nil
      (let* ((rurl (if (not (search "https://" url))
		  (concatenate 'string "https://" url)
		  url))
	     (pobj (puri:parse-uri rurl))
	     (host (puri:uri-host pobj))
	     (port (puri:uri-port pobj)))
	(format nil "https://~a:~a" host (if port port 443)))))

(defun get-cert (hash)
  "Get DER of cert."
  (when hash
      (handler-case
	  (gethash (binascii:decode-hex hash) *cert-hash-table*)
	(error (ex) (progn
		      (format t "Error ~a~%" ex)
		      nil)))))

(defun dump-cert (hash out)
  "Dump DER-formatted certificate with a given hex-encoded hash to file."
  (let ((der (get-cert hash)))
    (when der
	(with-open-file (stream out
				:if-exists :supersede
				:direction :output
				:element-type '(unsigned-byte 8))
	  (write-sequence der stream)))))

(defun list-certs ()
  "List certificates hex-encoded digest."
  (loop for key being the hash-keys of *cert-hash-table*
     collect (string-upcase (binascii:encode-hex key))))

(defun cert-names (hash)
  "Return certificate issuer and common name."
  (multiple-value-list
   (cl+ssl::x509-certificate-names
    (cl+ssl:decode-certificate :der (get-cert hash)))))

(defun cert-names-from-der (der)
  "Return certificate issuer and common name."
  (multiple-value-list
   (cl+ssl::x509-certificate-names
    (cl+ssl:decode-certificate :der der))))


(defun server-uptime ()
  "Return human-readable server uptime."
  (let ((sec (- (/ (get-internal-real-time) internal-time-units-per-second) *server-start-timestamp*)))
    (format nil "~d days ~d:~d:~d"
	    (floor sec 86400)
	    (mod (floor sec 3600) 24)
	    (mod (floor sec 60) 60)
	    (round (mod sec 60)))))


(hunchentoot:define-easy-handler (getcert :uri "/getcert") (hash)
  (if (or (null hash) (string= hash ""))
      nil
      (let ((cert (get-cert hash)))
	(setf (hunchentoot:content-type*) "application/pkix-cert")
	(setf (hunchentoot:content-length*) (length cert))
	cert)))

(hunchentoot:define-easy-handler (mitmcheck :uri "/mitmcheck") (url reload)
  (if (or (null url) (string= url ""))
      "nope"
      (let* ((rurl (normalize-url url))
	     (direct-digest (get-digest-direct rurl))
	     (upstream-override nil)
	     (upstream-digest nil)
	     (digest-chain nil))

	;; Check for upstream override
	(when *digest-callback*
	  (multiple-value-setq (upstream-override digest-chain)
			       (funcall *digest-callback* url direct-digest)))
	
	;; If there's upstream, fetch the hashes
	(if (and *upstream-server* (not upstream-override))
	    (progn
	      (multiple-value-bind (status-code headers)
		  (query-upstream rurl)
		(progn
		  (if (eq status-code 200)
		      (setf upstream-digest (drakma:header-value :HASH-CHAIN headers)))))
	      (if upstream-digest
		  (setf digest-chain (concatenate 'string upstream-digest ";" direct-digest))
		  (setf digest-chain (concatenate 'string ";" direct-digest))))
	    ;; If digest chain was not overridden, just set to direct digest
	    (unless digest-chain
	      (setf digest-chain direct-digest)))

	;; Set headers
	(setf (hunchentoot:content-type*) "text/plain")
	(setf (hunchentoot:header-out "Access-Control-Allow-Origin") "*")
	(if reload
	    (setf (hunchentoot:header-out "Cache-Control") "no-cache")
	    (setf (hunchentoot:header-out "Cache-Control")
		  (format nil "public, max-age=~a" *direct-cache-ttl*)))
	(setf (hunchentoot:header-out "Hash-Chain") digest-chain)

	;; Make verdict
	(if (get-verdict digest-chain)
	    (setf (hunchentoot:header-out "Verdict") "Trusted")
	    (setf (hunchentoot:header-out "Verdict") "Untrusted"))
	(when *verdict-callback*
	    (funcall *verdict-callback*
		     url
		     (hunchentoot:header-out :VERDICT)
		     (get-cert direct-digest)
		     digest-chain))
	(format nil "{\"url\":\"~a\",\"verdict\":\"~a\",\"hashchain\":\"~a\"}~%"
		url (hunchentoot:header-out :VERDICT) digest-chain))))


;;; Start server

(defun run (conf)
  (load conf)
  (if (and *key* *cert*)
      (hunchentoot:start (make-instance
			  'hunchentoot:easy-ssl-acceptor
			  :document-root nil
			  :port *port*
			  :address *address*
			  :access-log-destination *access-log*
			  :message-log-destination *message-log*
			  :ssl-privatekey-file *key*
			  :ssl-certificate-file *cert*))
      (hunchentoot:start (make-instance
			  'hunchentoot:easy-acceptor
			  :access-log-destination *access-log*
			  :message-log-destination *message-log*
			  :document-root nil
			  :address *address*
			  :port *port*))))


;;; Test the direct connection locally

(defun client (url &optional (conf nil))
  (when conf
    (load conf))
  (format t "~a - ~a~%" url (get-digest-direct (normalize-url url))))


;;; Start debug server

(defun run-debug (conf)
  (swank-loader:init)
  (swank:create-server :port 4007
		       :style swank:*communication-style*
		       :dont-close t)
  (run conf))


;;; Image builder

(defun build-image(&optional (image "mitmcheck"))
  (sb-ext:save-lisp-and-die image :executable t))

