FROM debian

# Install required packages
RUN apt-get update && \
    apt-get -y install \
	    sbcl \
	    curl \
	    sqlite3

# Add mitmcheck user
RUN groupadd -g 1000 mitmcheck
RUN useradd -rm -d /home/mitmcheck -s /bin/bash -g mitmcheck -u 1000 mitmcheck
USER mitmcheck
WORKDIR /home/mitmcheck

# Install quicklisp
RUN curl -O https://beta.quicklisp.org/quicklisp.lisp
RUN sbcl --load quicklisp.lisp \
         --eval '(quicklisp-quickstart:install)' \
         --eval '(quit)'

# This has the quicklisp init (don't know how to do it non-interactively)
ADD --chown=1000:1000 .sbclrc /home/mitmcheck/

# mitmcheck files
ADD --chown=1000:1000 start.sh /home/mitmcheck/
ADD --chown=1000:1000 build.sh /home/mitmcheck/
ADD --chown=1000:1000 mitmcheck.lisp /home/mitmcheck/

# Build mitmcheck
RUN ./build.sh

ENTRYPOINT ["./start.sh"]
