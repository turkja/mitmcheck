
# Mitmcheck

Mitmcheck (Man-In-The-Middle check) is experimental service that tries to detect SSL
MITM attacks. It is heavily inspired by a firefox add-on called [CheckMyHTTPS](https://checkmyhttps.net). Unfortunately, CheckMyHTTPS works no longer after Mozilla kills legacy add-on
API's and moves to WebExtensions. I thought the idea is so neat I have to do something
about it. And that's how Mitmcheck was born.

Mitmcheck offloads the browser certificate checks to external process, besides this
detail the idea is the same: compare local and remote view of the offered certificate.

It works by chaining up servers (two of them should be enough) and then the first server
(usually on localhost) makes a verdict out of the certificate chain. Add-on in the browser
is very light: just a background script doing Mitmcheck queries on each page load
and some glue code around it.


```
                 127.0.0.1                   1.2.3.4
 _________       _________                  __________
|         |     |         |                |          |
| Browser |---->|  Server |----->[...]---->| Upstream |
|_________|     |_________|                |__________|
     |               |                          |
     |               |                          |
 [Verdict] <---- [Compare] <-----[...]<---- [SSL-cert]

```

Mitmcheck should be considered a work-around while waiting for official WebExtensions
TLS inspection API's. It works quite well in the most typical MITM scenarios, but by
design it *cannot* detect the MITM if is being done inside the browser, as is the
case with some malware doing low-level browser API hooking.

AFAIK, there's some progress in in integrating TLS inspection to WebExtensions,
you can follow it [in this thread](https://bugzilla.mozilla.org/show_bug.cgi?id=1322748).


## Firefox Add-on

Mitmcheck system can be conveniently used with included browser add-on for firefox.
The Add-on functionality is very straightforward: it installs a popup with lock icon,
and the color of the icon indicates verdict. By clicking the icon, user can examine
some further details, for example there's a direct link to the certificate as it is
seen by the Mitmcheck server used by the add-on.


## Mitmcheck server(s)

The server is fairly standard HTTP(S) server ([Hunchentoot](http://weitz.de/hunchentoot/) -
web server written in Common Lisp). As a Lisp code, it is very flexible and convenient to
configure. The same server can take rather varying roles with a simple init configuration
file (see config.lisp and config-upstream.lisp for examples).

There are basically two different roles for servers: local entry server interfacing the
browser add-on (or any other client) and then a series of upstream servers. Each server
queries its upstream, and the last upstream server terminates this query chain.

Idea is two put at least one upstream server to some other network where the routing
is guaranteed to be different from what entry server is using.

In my setup, I have one server listening to 127.0.0.1 and then another one in the Internet.
These two then figure out something called "Verdict" - in essence, it is just a comparison
of SHA256 hashes of all the certificates in chain. If they are not all identical, verdict
is returned as "Untrusted". If they are identical, verdict "Trusted" is returned.

Note that if the upstream is down, verdict is Untrusted, because the entry server alone
cannot trust the certificate.


## How to install server(s)

Mitmcheck runs on SBCL, versions 1.2.x - 1.4.x have been somewhat tested on Linux x86_64.

There is no installation, really. I just use the script [build.sh](build.sh) for building
executable Lisp image. That is all.

Note that building should be done on each server instance as there are no
guarantees for the Lisp image working on different machines, even if the OS is
identical.

## Docker installation

For convenient deployment, there's an example Dockerfile that pulls debian and compiles
mitmcheck on it. The container can then be started like this:

```bash
$ docker build -t user/mitmcheck .
$ docker run -d -v `pwd`:/config --user mitmcheck -it user/mitmcheck /config/config.lisp
```

Notes:

* Above command runs the container in interactive mode in the background. That way you
  can attach to the container while mitmcheck server is running and issue commands
  to REPL.
* Dockerfile creates user 'mitmcheck' with 1000:1000 (id:gid) to the container to run
  the server. If that is not working for you, just change the numbers in Dockerfile.
* Volume in the build dir is just an example, in real life that should not be used.
* If the configuration lisp needs to write something, make sure `/config` is writable
  by id 1000 (or whatever user you want to run mitmcheck with - if you change that, you
  need to edit the Dockerfile)
* If your config requires additional quicklisp modules, they are compiled again on every
  start unless you make `/home/mitmcheck/quicklisp` as docker volume.
* If you use volume `/config`, as suggested above, make sure all the variables like
  certificate names, log files etc. also point to `/config`.

## How to configure and run server(s)

As a very experimental project, not much work has been put on how to deploy. I just
run them inside a screen session, and that's it. Works for me. Another option is to run
the servers in Docker containers (see above).

Running is done using a simple shell script [start.sh](start.sh) which just runs
the compiled Lisp image and loads a Lisp configuration file to the image. After this,
the server is up and running.

Check out example configurations config.lisp for local server and config-upstream.lisp
for upstream. The local server example writes verdicts and certificates to a sqlite3
database, so make sure to comment out that if not needed. In my config.lisp, I also
have other things like desktop notify with untrusted verdicts. The configuration file
gets information via `*verdict-callback*`.

Mitmcheck caches the upstream results with time-to-live in seconds controlled by
variable `*upstream-cache-ttl*`. By default this is set to 24 hours. It is a good
idea to have the cache to reasonable value because it makes no sense to do upstream
processing for each and every query. See notes below about caching.

*NOTE:* Upsteam server should *always* to use SSL. This is to ensure the verdict is
not tampered. With self-signed certificates and pinning, the querying server can be
quite sure it is talking to the right guy and simply drops connection if there is MITM.


### Callbacks

The server calls few callbacks, if defined. Most convenient way to define these callbacks
would be in the config.lisp.

* `*digest-callback*` - override upstream requests for given url
* `*verdict-callback*` - do additional logging
* `*proxy-callback*` - use proxy for given url

Examples of callback usage can be found in example [config.lisp](config.lisp)


## How to install browser add-on

There's a signed [XPI](add-on/ssl_mitm_check-0.2.0-an+fx.xpi) in directory add-on,
just install it using "Install Add-on From File" via Tools menu. The add-on is unlisted,
so it cannot be found from Firefox add-on public repository.


## Useful commands in the server REPL

* Check the contents of upstream cache: `(show-upstream-cache (&optional part-of-url))`
* Remove single entry from cache: `(remove-from-cache url)` ; url needs to be exactly
  what was reported by `(show-upstream-cache)`
* Flush the whole cache: `(clear-cache)`
* Change cache TTL: `(setf *upstream-cache-ttl* seconds)`
* List local certificate cache: `(list-certs)`
* Dump given certificate to disk: `(dump-cert hash out)` ; hash as reported by
  (list-certs), a full sha256 string
* Get cerfiticate from local cache as DER: `(get-cert hash)`
* Certificate name and issuer: `(cert-names)`, `(cert-names-from-der)`
* Test certificate direct connection: `(client url)`
* Server uptime: `(server-uptime)`
* Dump Lisp world and exit: `(build-image &optional name)`
* Exit: `(exit)`
* Unleash Battleship: `(any-s-expression)`

In the config.lisp there are some examples what could be done with the database.
For example:

```lisp
(cert-names (query-db-single
    "select local from verdicts where url like ?" "%google.com%"))
;-->
;("/C=US/O=Google Inc/CN=Google Internet Authority G2"
; "/C=US/ST=California/L=Mountain View/O=Google Inc/CN=*.google.com")
```

It should be noted that the database functions (`query-db`, `query-db-row`, `query-db-single`)
operate with on-disk database, which may or may not be in sync with the Mitmcheck
memory cache. So for example the above (cert-names) may not work. All the certificates
are actually recorded in the database, so in order to make sure to get the certificate
data one could write something like this:

```lisp
(cert-names-from-der
 (binascii:decode-hex (query-db-single
               "select hex(data) from certs where hash = ?"
               (query-db-single
                 "select local from verdicts where url like ?"
                 "%google.com%"))))
;-->
;("/C=US/O=Google Inc/CN=Google Internet Authority G2"
; "/C=US/ST=California/L=Mountain View/O=Google Inc/CN=*.google.com")
```

Another example: given a known hash, display the certificate issuer and subject names:

```lisp
CL-USER> (cert-names-from-db "B831F8A20E5BF2C6EA40...")
("/C=US/O=Google Inc/CN=Google Internet Authority G2"
 "/C=US/ST=California/L=Mountain View/O=Google Inc/CN=*.google.com")
```

Given a known hash, dump the certificate to disk as DER-formatted file:

```lisp
CL-USER> (dump-cert-from-db "B831F8A20E5BF2C6EA40..." "out.der")
```

This file can then be further analyzed with OpenSSL tools, like the example script
[certinfo.sh](certinfo.sh).


## Notes about caching

There are different kinds of caching happening in the Mitmcheck system.

### Local certificate cache

Mitmcheck servers maintains a cache of local cerficates while running. This is a
simple hash table mapping the certificate SHA256 hash to certificates in DER
format.

### Browser cache

Browser cache is controlled by the Mitmcheck server with standard Cache-Control
directives, with a little help from the browser add-on. Normal requests are cached
with a TTL defined by `*direct-cache-ttl*`, which is by default set to 300 seconds.

So things like browsing back and forth in the tab should not generate additional
Mitmcheck queries in 5 minutes.

But when user requests a page refresh, this condition is detected by the add-on
and additional command "reload=true" is added to the Mitmcheck request. Requests
with this command are always returned with "no-cache".

This seems to work pretty well at least with the latest Firefoxes.

### Upstream cache

Because the upstream is a baseline for certificate checks, it makes sense to have
relatively long caching for upstream requests. This is controlled by
`*upstream-cache-ttl*` and by default it is set to 24h. It could be even longer, maybe?

Obviously when the site changes certificate for legitimate reasons, upstream cache
needs to be cleared (see REPL examples above).

Upstream cache is a in-memory structure (Lisp hash table to be precise) so it is cleared
up in server restart.


## Some random notes

There's a one particular drawback in this approach when compared to original
CheckMyHTTPS: Mitmcheck cannot detect the MITM if it is *inside* the browser
because it is an external process. This also means that possible browser proxy
connections should be carefully reproduced (callback `*proxy-callback*`).

It also means that Mitmproxy should be configured to mimic browser behavior
when establishing SSL checks, meaning it should offer the same set of ciphers
etc. Mitmcheck tries to emulate recent firefox cipher set, but it may be not
completely identical so sneaky MITM could detect this condition.

I've also noticed that quite often legit sites use different certificates for
some reason, most likely something to do with renewing certs and having front
ends out of sync. SSL is happy with this condition, as long as the browser trusts
the CA's. But this happens every now and then, mostly with gigantic sites like google,
facebook, wikipedia etc. If real malicious MITM hits, one should see red Mitmcheck
icon on just about every site.


## Some totally tinfoil notes

While the fact Mitmcheck is run externally to the browser makes it a bit challenging
to get MITM'ed in some cases (see the random notes above), it also provides some
advantages. Because Mitmcheck is separated by the process boundary, it can initiate
for example desktop notifications independently of browser.

Actually, it would be best to not trust browser *at all*, including what the add-ons
are reporting. If the browser is infected - and it should be considered as such if
it runs javascript, flash, or any other ghetto code - it is very easy to just disable
add-ons or fake them.

Best solution to this problem would be to do the MITM checking on a trusted proxy, and
force all traffic through that, and then notify user completely outside the browser
context. Something like "antimitmproxy". The proxy could make a use of SNI to do the
certificate checking without being a MITM itself. Maybe some day...
