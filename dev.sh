#!/bin/sh

if [ ! $1 ]; then
    echo "Usage: run <config>"
    exit 1
fi


sbcl --load "mitmcheck.lisp" --eval "(run-debug' \"$1\")"
