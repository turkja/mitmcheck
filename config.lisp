
;;;; mitmcheck config

(ql:quickload 'sqlite)


;;; Cipher list (emulate firefox):

(setf *cipher-list* "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA:AES128-SHA:AES256-SHA:DES-CBC3-SHA")


;;; Local server
(setf *port* 24420)
(setf *address* "127.0.0.1")   ; Listen *only* to localhost interface

;;; Logging
(setf *access-log* *standard-output*)
(setf *message-log* *standard-output*)

;;; Upstream connection
(setf *upstream-server* "https://1.2.3.4:24420")  ; Replace with real IP
(setf *upstream-cert* "cert-upstream.pem")        ; Copied from upstream

;;; Upstream cache = 8h
(setf *upstream-cache-ttl* (* 8 60 60))

;;; Database for permanent certificate storage
(defvar *mitmcheck-database* nil)
(defvar *mitmcheck-database-name* "mitmcheck.db")


;; Enable command history
(defun enable-linedit ()
  (funcall (intern "INSTALL-REPL" :linedit) :wrap-current t))


;;; Database utilities

(defun open-database (&optional (name *mitmcheck-database-name*))
  (let ((path (probe-file name)))
    (setf *mitmcheck-database* (sqlite:connect name))
    (if (null path)
	(progn
	  (sqlite:execute-non-query
	   *mitmcheck-database*
	   "create table verdicts (time text primary key, url text, verdict text, upstream text, local text)")
	  (sqlite:execute-non-query
	   *mitmcheck-database*
	   "create table certs (hash text primary key, data blob)")))))

(defun close-database ()
  (sqlite:disconnect *mitmcheck-database*)
  (setf *mitmcheck-database* nil))

(defun insert-verdict (time url verdict upstream local)
  (if (or (null *mitmcheck-database*) (null (probe-file *mitmcheck-database-name*)))
      (open-database))
  (sqlite:execute-non-query
   *mitmcheck-database*
   "insert into verdicts (time, url, verdict, upstream, local) values (?, ?, ?, ?, ?)"
   time url verdict upstream local))

(defun insert-cert(hash data)
  (if (or (null *mitmcheck-database*) (null (probe-file *mitmcheck-database-name*)))
      (open-database))
  (if (null (sqlite:execute-single
   *mitmcheck-database*
   "select hash from certs where hash = ?" hash))
      (sqlite:execute-non-query
       *mitmcheck-database*
       "insert into certs (hash, data) values (?, ?)"
       hash data)))

(defun query-db (sql &rest values)
  "Execute SQL query, return rows as list of lists."
  (apply #'sqlite:execute-to-list
	 (cons *mitmcheck-database* (cons sql values))))

(defun query-db-row (sql &rest values)
  "Execute SQL query, return first row as values."
  (apply #'sqlite:execute-one-row-m-v
	 (cons *mitmcheck-database* (cons sql values))))

(defun query-db-single (sql &rest values)
  "Execute SQL query, return first value of the first row."
  (apply #'sqlite:execute-single
	 (cons *mitmcheck-database* (cons sql values))))

(defun cert-names-from-db (hash)
  "Display certificate names from database."
  (cert-names-from-der
   (binascii:decode-hex (query-db-single
               "select hex(data) from certs where hash = ?" hash))))

(defun dump-cert-from-db (hash out)
  "Dump certificate from database."
  (let ((der (binascii:decode-hex (query-db-single
               "select hex(data) from certs where hash = ?" hash))))
    (when der
	(with-open-file (stream out
				:if-exists :supersede
				:direction :output
				:element-type '(unsigned-byte 8))
	  (write-sequence der stream)))))


(defun hash-chain-as-table (hash-chain)
  "Split the chain of hashes to table."
  (split-sequence:split-sequence #\; hash-chain))


;;; XXX: prepare for corner case where upstream goes belly up
;;; between local server query and this call
(defun get-upstream-cert (hash)
  "Fetch certificate from upstream."
  (if (not (string= "" hash))  ; empty string means no upstream
      (let ((body (drakma:http-request
	       (concatenate 'string *upstream-server* "/getcert?hash=" hash)
	       :ca-file *upstream-cert*)))
	(if (and (stringp body) (string= body ""))
	    nil
	    body))
      nil))


;;; Verdict callback
;;; Here we do all the logging

(defun my-verdict-callback (url verdict der hash-chain)
  ;; Log the verdict
  (insert-verdict
   (local-time:format-timestring nil (local-time:now))
   url
   verdict
   (first (hash-chain-as-table hash-chain))
   (second (hash-chain-as-table hash-chain)))

  ;; Log also certificate (if given)
  (when der
      (insert-cert (second (hash-chain-as-table hash-chain)) der))
  
  ;; If the verdict is Untrusted, check if there's an upstream hash
  ;; if yes - fetch it directly from the upstream
  (if (string= verdict "Untrusted")
      (let* ((hash (first (hash-chain-as-table hash-chain)))
	     (der2 (get-upstream-cert hash)))
	(when der2
	    (insert-cert hash der2)))))

;;; Digest callback

;; In this example, just always trust "common-lisp.net" providing trusted
;; certificate. Useful for preventing leaks of intra server names to upstream.
(defun my-digest-callback (url digest)
  (if (search "common-lisp.net" url)
      (values T (concatenate 'string digest ";" digest))
      nil))

;;; Proxy callback should faithfully replicate what is expected from the browser

(defun my-proxy-callback (url) nil)

;;; Set callbacks
(setf *verdict-callback* 'my-verdict-callback)
(setf *proxy-callback* 'my-proxy-callback)
(setf *digest-callback* 'my-digest-callback)

;;; Open the database
(open-database)


