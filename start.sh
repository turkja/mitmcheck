#!/bin/sh

if [ ! $1 ]; then
    echo "Usage: $0 <config>"
    exit 1
fi

./mitmcheck --eval "(run \"$1\")"
