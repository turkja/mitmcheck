

var mitmCheckURL;

function setMitmcheckURL (url) {
    console.log("Setting url: " + url);
    mitmCheckURL = url;
}
function onError(error) {
    console.log(`Error: ${error}`);
}
function onGot(item) {
    if (item.mitmcheck === undefined) {
	var defaultURL = "http://127.0.0.1:24420";
	browser.storage.local.set({
	    mitmcheck: defaultURL
	});
	setMitmcheckURL(defaultURL);
    }
    else
	setMitmcheckURL(item.mitmcheck);
}
var getting = browser.storage.local.get("mitmcheck");
getting.then(onGot, onError);


var last_verdict = {};

function getVerdict(id) {
    return last_verdict[id];
}

function setStatusBlue(url, id) {
    browser.browserAction.setTitle({tabId: id, title: "Mitmcheck: " + url + ": Checking"});
    browser.browserAction.setIcon({tabId: id, path: "icons/blue.png"});
}
function setStatusRed(url, id) {
    browser.browserAction.setTitle({tabId: id, title: "Mitmcheck: " + url + ": Untrusted"});
    browser.browserAction.setIcon({tabId: id, path: "icons/red.png"});
}
function setStatusGreen(url, id) {
    browser.browserAction.setTitle({tabId: id, title: "Mitmcheck: " + url + ": Trusted"});
    browser.browserAction.setIcon({tabId: id, path: "icons/green.png"});
}
function setStatusGray(url, id) {
    browser.browserAction.setTitle({tabId: id, title: "Mitmcheck: " + url + ": Unknown"});
    browser.browserAction.setIcon({tabId: id, path: "icons/grey.png"});
}

browser.webNavigation.onCommitted.addListener(evt => {

    // Filter out any sub-frame related navigation event
    if (evt.frameId !== 0) {
	return;
    }

    const url = new URL(evt.url);
    setStatusBlue(url.hostname, evt.tabId);

    if (url.protocol == "https:") {
	const httpRequest = new XMLHttpRequest();

	httpRequest.onerror = function () {
	    last_verdict[evt.tabId] = {};
	    setStatusGray(url.hostname, evt.tabId);
	};
	
	httpRequest.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
		// Typical action to be performed when the document is ready:
		last_verdict[evt.tabId] = JSON.parse(this.response);
		if (last_verdict[evt.tabId].verdict == "Trusted") {
		    setStatusGreen(url.hostname, evt.tabId);
		} else {
		    setStatusRed(url.hostname, evt.tabId);
		}
	    }
	};

	var queryURL = "https://" + url.hostname;
	if (url.port)
	    queryURL += ":" + url.port;

	var commands = "";
	if (evt.transitionType == "reload")
	    commands = "/mitmcheck?reload=true&url=";
	else
	    commands = "/mitmcheck?url=";
	httpRequest.open('GET', mitmCheckURL + commands + queryURL, true);
	httpRequest.send();
    } else {
	last_verdict[evt.tabId] = {};
	last_verdict[evt.tabId].url = evt.url;
	last_verdict[evt.tabId].verdict = "Not HTTPS";
	setStatusGray(url.hostname, evt.tabId);
    }
}, {
    url: [{schemes: ["http", "https"]}]});

