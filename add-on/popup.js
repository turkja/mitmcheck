

var mitmCheckURL;

function setMitmcheckURL (url) {
    mitmCheckURL = url;
}
function onError(error) {
    console.log(`Error: ${error}`);
}
function onGot(item) {
    setMitmcheckURL(item.mitmcheck);
}
var getting = browser.storage.local.get("mitmcheck");
getting.then(onGot, onError);


function logTabs(tabs) {
    for (let tab of tabs) {

	var page = browser.extension.getBackgroundPage();
	var verdict = page.getVerdict(tab.id);

	let listEl = document.querySelector("ul");
	while(listEl.firstChild)
	    listEl.removeChild(listEl.firstChild);
    
	var listItem = document.createElement("li");
	if (verdict.url === undefined) {
	    listItem.textContent = "url: (connection error)";
	} else {
	    listItem.textContent = "url: " + verdict.url;
	}
	listEl.appendChild(listItem);

	var listItem = document.createElement("li");
	listItem.textContent = "verdict: " + verdict.verdict;
	
	if (verdict.verdict == "Trusted") {
	    listItem.style.color = 'green';
	    document.querySelector("#mitmcheckStatus").style.color = 'green';
	} else if (verdict.verdict == "Untrusted") {
	    listItem.style.color = 'red';
	    document.querySelector("#mitmcheckStatus").style.color = 'red';
	}
	listEl.appendChild(listItem);

	var hash = verdict.hashchain.split(";")[1];
	var listItem = document.createElement("li");
	if ((hash == "") || (hash == undefined)) {
	    listItem.textContent = "certificate not available";
	} else {
	    var newAnchor = document.createElement("a");
	    newAnchor.textContent = "certificate";
	    newAnchor.setAttribute('href', mitmCheckURL + "/getcert?hash=" + hash);
	    listItem.appendChild(newAnchor);
	}
	listEl.appendChild(listItem);
    }
}

function onError2(error) {
  console.log(`Error: ${error}`);
}

var querying = browser.tabs.query({currentWindow: true, active: true});
querying.then(logTabs, onError2);



