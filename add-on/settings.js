function saveOptions(e) {
  e.preventDefault();
  browser.storage.local.set({
    mitmcheck: document.querySelector("#mitmcheck").value
  });
  var page = browser.extension.getBackgroundPage();
  page.setMitmcheckURL(document.querySelector("#mitmcheck").value);
}

function restoreOptions() {

  function setCurrentChoice(result) {
      document.querySelector("#mitmcheck").value = result.mitmcheck;
  }
  function onError(error) {
    console.log(`Error: ${error}`);
  }

  var getting = browser.storage.local.get("mitmcheck");
  getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
