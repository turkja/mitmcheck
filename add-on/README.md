# mitmcheck firefox add-on

This add-on is based on MDN example [navigation stats](https://github.com/mdn/webextensions-examples/tree/master/navigation-stats).

It presents a colored lock icon based on tab's MITM situation:

- Gray: no HTTPS or Mitmcheck connection failure
- Blue: fetching results from Mitmcheck server
- Green: Trusted certificate
- Red: Untrusted certificate

Hover the mouse over icon or click it to get more information about the Mitmcheck status.

If the status is gray and there's a connection failure, make sure that your Mitmcheck
server matches the add-on settings, which you can find using the Firefox add-on preferences
page.

