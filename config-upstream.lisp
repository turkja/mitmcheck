
;; mitmcheck config

;; Local server
(setf *port* 24420)
(setf *address* "1.2.3.4")           ; Listen *only* to specific interface
(setf *key* "key-upstream.pem")      ; needs to be generated
(setf *cert* "cert-upstream.pem")    ; needs to be generated + copied to main server

;;; Logging
(setf *access-log* *standard-output*)
(setf *message-log* *standard-output*)


