#!/bin/sh

if [ ! $1 ]; then
    echo "Usage: client <url>"
    exit 1
fi


./mitmcheck --eval "(client \"$1\" \"config-client.lisp\")"

