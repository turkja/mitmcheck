#!/bin/sh

if [ ! $1 ]; then
    echo "Usage: certinfo <der>"
    exit 1
fi

openssl x509 -in $1 -inform der -noout -text

